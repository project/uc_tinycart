README.txt for UC Tiny Cart v7.x-1.x

Overview
--------
This module simply exposes a tiny, icon-based block for the Ubercart shopping
cart with an item count. When hovered, the shopping cart contents appear in a 
div overlay. The effect is achieved via CSS and a little jQuery.
